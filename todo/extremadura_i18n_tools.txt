During the Extremadura 2007 I18N Meeting, we had a look at a new i18n related
software: transifex.

This session was extended to a review of what the other distributions use
to manage their translations, and see if we were lagging behind and how we
could improve.

This session was limited in time, and thus some of the below information
may not be as accurate as we would like.
If you have more information or more accurate information, do not hesitate to
send corrections to debian-i18n@lists.debian.org

Here is the report of this session, which include a description of this new
tools and analysis on how it could be used by Debian.
The reports ends by a summary of what we could find regarding the l10n
infrastructure of other distribution.

A separate report try to describes some area were the Debian i18n
infrastructure could be enhanced.

Transifex
=========
HomePage: https://hosted.fedoraproject.org/projects/transifex/

Description (from the HomePage)
-------------------------------
    Transifex is a web-system that facilitates the process of submitting
    translations in various version control systems (VCS). The name literally
    means "translation-builder" (from the latin verb facere).

Description
-----------
Transifex is integrated in Damned Lies and provides the access methods to
various Version Control Systems (VCS).
It was developed by Dimitris Glezos during a Google Summer of Code project,
mentored by Fedora.

It is (being) adopted by the Fedora project
(http://translate.fedoraproject.org/), but we could not test more since it
requires login access.

The Fedora Localization Project (FLP, http://translate.fedoraproject.org/) has
large localization teams, and want to give them the possibility to provide
translation to upstream maintainers.
The Fedora Project also supports a lot of different VCS for the Fedora
specific tools/packaging, and some packages are hosted out of the Fedora
infrastructure, but Fedora translator have only access to the Fedora's CVS
repository.

It (Transifex and Damned Lies) is a central concentration point for
translation material:
 * it provides translation materials to translators, gathered from upstream
   repositories
 * it provides statistics about the current translations
 * it accept updated material and commit them to the upstream repositories
   Needs accounts tuning
     - transifex has an account on the various repositories (ssh account)
     - translators are authenticated by transifex (OpenID planned,
       currently Fedora authentication)
 * it notifies the translators (planned)

To commit the updated translation, an account must be created for transifex
(ssh account). The translators are authenticated by transifex (OpenID planed,
currently users are authenticated by the Fedora system).

This removes the need for translators to use and understand various VCS
systems.
It helps developers to find translators if a large community of translators
are using transifex.
It avoids the creation of too many accounts on the VCS.

Other related tools
-------------------
  * mr (Multi Repository): http://kitenet.net/~joey/code/mr/
    The mr(1) command can checkout, update, or perform other actions on a set
    of repositories as if they were one combined repository. It supports any
    combination of subversion, git, cvs, mercurial, bzr and darcs repositories,
    and support for other revision control systems can easily be added.

    We could not test the "commit" feature of transifex, but it seems it is a
    web page allowing the upload of a file, which should be even more simple
    than using mr.

  * TP (TranslationProject): http://translationproject.org/html/welcome.html
    The Translation Project (TP for short) aims to get maintainers and
    translators of free software packages together, so that most computer
    programs gradually become able to speak many languages.

    transifex differs a bit from the TP because the PO files are pulled
    by transifex (we did not check the frequency), while the Translation
    Project relies on the upstream authors which must push their PO files when
    they are ready to be translated (before a release).
    Both methods have pros and cons.

  * Pootle
    Pootle is also a central repository of translation material. It can be
    integrated to subversion repositories (this was not tested a lot on the
    Debian Pootle instance), and support for other VCS systems is being
    integrated upstream.
    Pootle also supports web based translation, which is a nice feature, but
    makes also the system more complex (the content is much more dynamic).

Unknown behavior
----------------
  * reservation of a PO file
  * merges
  * allowing upstream translators not using transifex
  * using multiple instances of transifex
    They would need to be synchronized, in order to share the same translation
    teams. OpenID would be needed.


Possible usage in Debian
------------------------
The goal of supporting upstream translation does not seems to be shared by
Debian translation teams, which focus on the translation of Debian specific
messages.

But Debian differs from other distribution by its heavily decentralized
nature, and could benefit from support of multiple VCS, and Debian
developers could like the delegation of commit rights to a transifex server,
which could be supported by the Debian development infrastructure (e.g. on
Alioth).

Personal thoughts
-----------------
transifex is still not complete. It still needs 2 major milestone before
public readiness (https://hosted.fedoraproject.org/transifex/roadmap).
It is currently in production for Fedora, but I don't know if it is currently
used.


Damned Lies
===========
Damned Lies (DL) is a web service developed for the Gnome localization teams.

It permits to manage teams and translations:
 * List of teams
   This provides contact information, processes of the team, mailing list,
   links
   A team can manage multiple languages.
   It provides a summary of the translation statistics is displayed for the
   languages and releases supported by the team.
 * List of languages
 * List of releases
   A release is a set of packages and their versions.
   It could be for Debian sid, testing, stable, but also debian-med, etc.
 * List of packages
   It provides links to the package, VCS, and description
   It provides the statistics of the translations, and the translation material
   (POT, POs) for the releases supported by the package.

Along with the downloadable PO files, translator can see warnings or error of
the PO files.

The web interface is nice and quite intuitive. It can provide access to the
translation materials, and information how to handle the translation, how to
report it, etc.

Damned Lies is currently used in production by Fedora, and Gnome.

It could probably be deployed easily for Debian (the main point is to gather
the translation material. Then links to packages.d.o could be added.


Summary
=======
DL looks nice and could easily be deployed to replace the www.debian.org
pages if there is an interest.

transifex has some nice features, but still seems too young and will need more
love.

This is yet another i18n infrastructure tool



Summary of the other distribution's infrastructure
==================================================
Fedora
------
Fedora has a centralized (multi-packages, multi-teams) i18n infrastructure.

Fedora uses (or will use) DL + transifex (http://translate.fedoraproject.org/)

Finding information on the Fedora processes was quite simple.

RedHat
------
Will move to the Fedora infrastructure

Gnome
-----
Gnome uses Damned Lies

Some Gnome teams have additional layers to handle workflow (e.g.
http://gnomefr.traduc.org/suivi)

Translators have access to statistics and PO files.
The single project nature probably helps translators pushing their
translations.

KDE
---
KDE uses l10n-stats (http://l10n.kde.org/teams-list.php,
http://l10n.kde.org/about-stats.php)

It is a web interface similar to DL.
The team information also contain the list of members of a team.
It differentiate a team administrator from team coordinator and translator.

The statistics support statistics history. Statistics for the applications and
for the documentation are separated.

l10N-stats is used by KDE and compiz.

Novel/Suse
----------
The Suse l10n infrastructure (http://i18n.opensuse.org/) seems to be based on
a Mediawiki adaptation.

It provides howtos, list of teams, statistics, access to PO files, SVN
information.

The processes seems more simple due to a more centralized development
infrastructure
(http://developer.novell.com/wiki/index.php/Suse-i18n-downloads).

Mandriva
--------
 * Not found

Gentoo
------
The infrastructure is focused on the translation of documentation.

It is Wiki based.

Ubuntu
------
The l10N work on Ubuntu is done in Rosetta/Launchpad.

Debian
------
Finding information with Google was not that easy.
Maybe it was too late, and I could hardly open my eyes, maybe the
documentation should receive more advertisement.

